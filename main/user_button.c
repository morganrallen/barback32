#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/gpio.h"

#include "main.h"
#include "pumps.h"
#include "ble.h"

#define GPIO_USER_BUTTON      (19)

static void IRAM_ATTR gpio_isr_handler(void* arg) {
  uint32_t gpio_num = (uint32_t) arg;

  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}

static void gpio_task(void* arg) {
  uint32_t io_num;
  uint8_t state = 0;
  int8_t last_level = -1;

  for(;;) {
    if(xQueueReceive(gpio_evt_queue, &io_num, portMAX_DELAY)) {
      uint8_t level = gpio_get_level(io_num);

      if(level == last_level) continue;
      last_level = level;

      printf("GPIO[%d] intr, val: %d\n", io_num, level);

      if(state == 0)
        vTaskDelay(10 / portTICK_RATE_MS);

      pumps_run();

      state = !state;

      ble_send_notification((void*)&state, 1);
    }
  }
}

void user_button_init() {
  gpio_config_t io_conf;

  io_conf.intr_type = GPIO_PIN_INTR_ANYEDGE;
  io_conf.mode = GPIO_MODE_INPUT;
  io_conf.pin_bit_mask = (1ULL << GPIO_USER_BUTTON);
  io_conf.pull_down_en = 0;
  io_conf.pull_up_en = 1;

  gpio_config(&io_conf);

  gpio_evt_queue = xQueueCreate(1, sizeof(uint32_t));
  xTaskCreate(gpio_task, "gpio_task", 4096, NULL, 10, NULL);

  gpio_install_isr_service(0);
  gpio_isr_handler_add(GPIO_USER_BUTTON, gpio_isr_handler, (void*)GPIO_USER_BUTTON);
}
