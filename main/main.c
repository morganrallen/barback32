#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "esp_debug_helpers.h"
#include "nvs_flash.h"
#include "esp_vfs_dev.h"

#include "main.h"
#include "console.h"
#include "ble.h"
#include "user_button.h"
#include "pumps.h"

static const char *TAG = "BARBACK";

uint8_t mac[6];

void app_main(void) {
  esp_err_t err = nvs_flash_init();

  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      err = nvs_flash_init();
  }
  ESP_ERROR_CHECK( err );

  err = nvs_open("config", NVS_READWRITE, &config_handle);
  if (err != ESP_OK) {
    printf("Error (%s) opening NVS handle!\n", esp_err_to_name(err));
  } else {
    ESP_LOGI(TAG, "config_handle: %d", config_handle);
  }

  esp_efuse_mac_get_default((uint8_t*)&mac);

  ESP_LOGI(TAG, "MAC: [%02X:%02X:%02X:%02X:%02X:%02X]", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  char *name = malloc(strlen(TAG) + 4);
  sprintf(name, "%s-%02X", TAG, mac[5]);

  user_button_init();
  pumps_init();
  ble_init(name);

  xTaskCreate(console_task, "console", 4048, NULL, tskIDLE_PRIORITY + 3, NULL);

  while(1) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);
  }
}
