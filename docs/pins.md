# Pin Function Table

```
 GPIO | Function | Notes
------|----------|-----------
  27  |  PUMP_1  | 
  26  |  PUMP_2  |
  25  |  PUMP_3  |
  33  |  PUMP_4  |
  19  |  USER_1  | Thumb Butt
  32  |  SPOUT   |
  14  |  GLOVE   |
```

# IO Requirements
