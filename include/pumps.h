#ifndef __PUMPS_H
#define __PUMPS_H

#include "stdint.h"

#define PUMPS               (4)
#define PUMPS_ERR_OUT_IDX   (1)
#define GPIO_PUMP_PIN_SEL   ((1ULL<<pump_gpio_map[0]) | (1ULL<<pump_gpio_map[1]) |  (1ULL<<pump_gpio_map[2]) | (1ULL<<pump_gpio_map[3]));

static uint8_t pump_gpio_map[] = { 27, 26, 25, 33 };

void pumps_run();
void pumps_stop();
uint8_t pumps_init();

uint8_t pumps_set_duration(uint8_t idx, uint8_t time);
uint8_t pumps_get_duration(uint8_t idx);

uint8_t pumps_set_enabled(uint8_t idx, uint8_t value);
uint8_t pumps_get_enabled(uint8_t idx);

uint8_t pumps_get_state(uint8_t idx);

#endif
