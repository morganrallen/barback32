#ifndef __CMD_SETTINGS_H_
#define __CMD_SETTINGS_H_

#ifdef __cplusplus
extern "C" {
#endif

static struct wifi_settings {
  uint8_t enabled;
  uint8_t mode; // 0 = station, 1 = AP
  char    *ssid;
  char    *password;
};

static struct {
  struct wifi_settings wifi;
} lorcomm_settings;

uint8_t init_settings_cmds();

#ifdef __cplusplus
}
#endif
#endif
