#ifndef __BLE_H_H
#define __BLE_H_H

typedef int (*event_callback_t)(struct ble_gap_event *, void *);

typedef struct {
  uint32_t *handle;
  event_callback_t callback;
} event_callback_handle_t;

int8_t ble_send_notification(void *buf, uint8_t size);
void ble_init(char *name);

#endif
