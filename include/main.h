#ifndef __MAIN_H_
#define __MAIN_H_

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "nvs_flash.h"

#define PUMP_COUNT		(4)

static QueueHandle_t gpio_evt_queue = NULL;
nvs_handle_t config_handle;

extern uint8_t mac[6];

#endif
