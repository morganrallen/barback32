#ifndef __BARBACK_H__
#define __BARBACK_H__

typedef uint8_t (*barback_ble_access_read_t)(uint8_t idx);
typedef uint8_t (*barback_ble_access_write_t)(uint8_t idx, uint8_t value);

typedef struct {
  uint16_t base_uuid;

  barback_ble_access_read_t read;
  barback_ble_access_write_t write;
} barback_ble_access_t;

#endif
